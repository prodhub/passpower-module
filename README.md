#PassPower

Password power module


## Install

```sh
$ npm install --save @adwatch/passpower
```


## Usage

```js
import PassPower from '@adwatch/passpower';
// or
var PassPower = require('@adwatch/passpower/build');
```


## API


####passScore(val, minPassLength, analysis)

Get password score and return common information about it

**val**

Type `string`

**minPassLength**

Type `number`

**analysis**

Type `array`

Default `[]`

```js
console.log(PassPower.passScore('987654321Qq', 8, ['hasUppercase', 'hasLowercase']));
```


####setScorePassword(val, analyse, length)

Analyse your password and return it power

**val**

Type `string`

**analyse**

Type `object`

**length**

Type `number`

```js
let val = '987654321Qq';

let result = {
	hasUppercase: /[A-Z]/.test(val),
	hasLowercase: /[a-z]/.test(val),
	hasDigits: /[0-9]/.test(val),
	hasSpecials: /[_\W]/.test(val)
};

console.log(PassPower.setScorePassword(val, result, 8));
```


## License

MIT ©
